import { Express } from 'express';
export default class App {
    name: string;
    port: number;
    server: Express;
    constructor();
    run(): Promise<void>;
    loadControllers(): Promise<void>;
}
