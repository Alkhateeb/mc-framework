"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const Controller_1 = __importDefault(require("../controller/Controller"));
const glob_1 = __importDefault(require("glob"));
const path_1 = __importDefault(require("path"));
class App {
    constructor() {
        this.name = 'app';
        this.port = 3000;
        this.server = express_1.default();
        this.server.use(express_1.default.json());
    }
    run() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.loadControllers();
            this.server.listen(this.port, () => {
                console.log(`Running application ${this.name} on http://localhost:${this.port}`);
            });
        });
    }
    loadControllers() {
        return __awaiter(this, void 0, void 0, function* () {
            const paths = glob_1.default.sync('**/*.controller.js');
            yield Promise.all(paths.map(p => Promise.resolve().then(() => __importStar(require(path_1.default.resolve(process.cwd(), p))))));
            this.server.use('/', Controller_1.default.router);
        });
    }
}
exports.default = App;
