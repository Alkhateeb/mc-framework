import { Request, Response } from "express";
export default abstract class Controller {
    static router: import("express-serve-static-core").Router;
    static registerRoute(type: 'GET' | 'POST', route: string, method: Function): void;
    static getHandler(method: any): (req: Request, res: Response) => Promise<void>;
    static postHandler(method: Function): (req: Request, res: Response) => Promise<void>;
    constructor();
}
