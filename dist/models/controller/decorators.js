"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Post = exports.Get = void 0;
const Controller_1 = __importDefault(require("./Controller"));
require("reflect-metadata");
function Get(target, key, descriptor) {
    if (target instanceof Controller_1.default) {
        // const test = 'sss'
        // const params = Reflect.getMetadata("design:paramtypes", target, key!)
        // console.log(params);
        // console.log(Reflect.getMetadata("design:type", target, key!));
        // // [Function: Function]
        // // Checks the types of all params
        // // [[Function: Number]]
        // // Checks the return type
        // console.log(Reflect.getMetadata("design:returntype", target, key!));
        const controllerName = target.constructor.name.replace('Controller', '');
        const route = `/${controllerName}/${key}`;
        const method = descriptor === null || descriptor === void 0 ? void 0 : descriptor.value;
        Controller_1.default.registerRoute("GET", route, method);
    }
    else {
        return function (constructor, key, descriptor) {
            const route = target.startsWith('/') ? target : `/${target}`;
            const method = descriptor === null || descriptor === void 0 ? void 0 : descriptor.value;
            Controller_1.default.registerRoute("GET", route, method);
        };
    }
}
exports.Get = Get;
function Post(target, key, descriptor) {
    if (target instanceof Controller_1.default) {
        const controllerName = target.constructor.name.replace('Controller', '');
        const route = `/${controllerName}/${key}`;
        const method = descriptor === null || descriptor === void 0 ? void 0 : descriptor.value;
        Controller_1.default.registerRoute("POST", route, method);
    }
    else {
        return function (constructor, key, descriptor) {
            const route = target.startsWith('/') ? target : `/${target}`;
            const method = descriptor === null || descriptor === void 0 ? void 0 : descriptor.value;
            Controller_1.default.registerRoute("POST", target, method);
        };
    }
}
exports.Post = Post;
