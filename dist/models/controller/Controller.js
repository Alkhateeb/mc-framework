"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
let Controller = /** @class */ (() => {
    class Controller {
        constructor() {
        }
        static registerRoute(type, route, method) {
            if (method) {
                if (type === 'GET') {
                    this.router.get(route, this.getHandler(method));
                }
                else if (type === 'POST') {
                    this.router.post(route, this.postHandler(method));
                }
            }
        }
        static getHandler(method) {
            return (req, res) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const returnValue = yield method();
                    const code = returnValue === undefined ? 204 : 200;
                    res.status(code).json(returnValue);
                }
                catch (error) {
                    console.log(error);
                    res.status(400).json();
                }
            });
        }
        static postHandler(method) {
            return (req, res) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const returnValue = yield method();
                    const code = returnValue === undefined ? 204 : 200;
                    res.status(code).json(returnValue);
                }
                catch (error) {
                    console.log(error);
                }
            });
        }
    }
    Controller.router = express_1.Router();
    return Controller;
})();
exports.default = Controller;
