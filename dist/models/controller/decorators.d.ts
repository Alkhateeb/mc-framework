import Controller from "./Controller";
import 'reflect-metadata';
declare type MethodDescriptor = TypedPropertyDescriptor<any>;
export declare function Get<T extends Controller>(target: T, key: string, descriptor: MethodDescriptor): void;
/**Will override the default routing */
export declare function Get<T extends Controller>(route: string): (target: T, key: string, descriptor: MethodDescriptor) => void;
export declare function Post<T extends Controller>(target: T, key: string, descriptor: MethodDescriptor): void;
/**Will override the default routing */
export declare function Post<T extends Controller>(route: string): (target: T, key: string, descriptor: MethodDescriptor) => void;
export {};
