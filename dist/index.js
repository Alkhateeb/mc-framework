"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const App_1 = __importDefault(require("./models/app/App"));
exports.default = App_1.default;
var Controller_1 = require("./models/controller/Controller");
Object.defineProperty(exports, "Controller", { enumerable: true, get: function () { return Controller_1.default; } });
var decorators_1 = require("./models/controller/decorators");
Object.defineProperty(exports, "Get", { enumerable: true, get: function () { return decorators_1.Get; } });
Object.defineProperty(exports, "Post", { enumerable: true, get: function () { return decorators_1.Post; } });
