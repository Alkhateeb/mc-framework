import Controller from "./Controller";
import 'reflect-metadata'
type MethodDescriptor = TypedPropertyDescriptor<any>

// Object.defineProperty(Object.prototype, '_object', { get: function () { return this; } });

export function Get<T extends Controller>(target: T, key: string, descriptor: MethodDescriptor): void
/**Will override the default routing */
export function Get<T extends Controller>(route: string): (target: T, key: string, descriptor: MethodDescriptor) => void
export function Get<T extends Controller>(target: string | T, key?: string, descriptor?: MethodDescriptor) {
    if (target instanceof Controller) {

        // const test = 'sss'
        // const params = Reflect.getMetadata("design:paramtypes", target, key!)
        // console.log(params);

        // console.log(Reflect.getMetadata("design:type", target, key!));
        // // [Function: Function]
        // // Checks the types of all params
        // // [[Function: Number]]
        // // Checks the return type
        // console.log(Reflect.getMetadata("design:returntype", target, key!));





        const controllerName: string = target.constructor.name.replace('Controller', '')
        const route: string = `/${controllerName}/${key}`
        const method = descriptor?.value
        Controller.registerRoute("GET", route, method)

    } else {
        return function (constructor: T, key: string, descriptor: MethodDescriptor) {
            const route: string = target.startsWith('/') ? target : `/${target}`
            const method = descriptor?.value
            Controller.registerRoute("GET", route, method)

        }
    }
}

export function Post<T extends Controller>(target: T, key: string, descriptor: MethodDescriptor): void
/**Will override the default routing */
export function Post<T extends Controller>(route: string): (target: T, key: string, descriptor: MethodDescriptor) => void
export function Post<T extends Controller>(target: string | T, key?: keyof T, descriptor?: MethodDescriptor) {
    if (target instanceof Controller) {
        const controllerName: string = target.constructor.name.replace('Controller', '')
        const route: string = `/${controllerName}/${key}`
        const method = descriptor?.value
        Controller.registerRoute("POST", route, method)

    } else {
        return function (constructor: T, key: string, descriptor: MethodDescriptor) {
            const route: string = target.startsWith('/') ? target : `/${target}`
            const method = descriptor?.value
            Controller.registerRoute("POST", target, method)

        }
    }
}




