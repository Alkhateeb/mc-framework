import { Router, Request, Response } from "express";


export default abstract class Controller {
    static router = Router()
    static registerRoute(type: 'GET' | 'POST', route: string, method: Function) {
        if (method) {
            if (type === 'GET') {
                this.router.get(route, this.getHandler(method))
            } else if (type === 'POST') {
                this.router.post(route, this.postHandler(method))
            }
        }
    }

    static getHandler(method: any) {
        return async (req: Request, res: Response) => {
            try {
                const returnValue = await method()
                const code = returnValue === undefined ? 204 : 200
                res.status(code).json(returnValue)
            } catch (error) {
                console.log(error)
                res.status(400).json()
            }
        }
    }

    static postHandler(method: Function) {
        return async (req: Request, res: Response) => {
            try {
                const returnValue = await method()
                const code = returnValue === undefined ? 204 : 200
                res.status(code).json(returnValue)
            } catch (error) {
                console.log(error)
            }
        }
    }
    constructor() {
    }

}

