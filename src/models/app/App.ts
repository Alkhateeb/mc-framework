import express, { Express } from 'express'
import Controller from '../controller/Controller'
import glob from 'glob'
import path from 'path'

export default class App {
    name: string = 'app'
    port: number = 3000
    server: Express = express()
    constructor() {

        this.server.use(express.json())
    }


    async run() {
        await this.loadControllers()

        this.server.listen(this.port, () => {
            console.log(`Running application ${this.name} on http://localhost:${this.port}`)
        })

    }

    async loadControllers() {
        const paths = glob.sync('**/*.controller.js')
        await Promise.all(paths.map(p => import(path.resolve(process.cwd(), p))))
        this.server.use('/', Controller.router)
    }

}