import Controller from "../models/controller/Controller";
import { Get, Post } from "../models/controller/decorators";


export default class ItemsController extends Controller {

    constructor() {
        super()

    }

    @Get('test/')
    async getMethodAsync(name: string) {

        return 'getMethodAsync'
    }

    @Get
    getMethod(name: string, age: number): string {

        return 'not async'
    }
    @Post
    postMethod(data: string) {

    }
}